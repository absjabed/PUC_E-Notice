package com.example.absjabed.puc_e_notice;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class show_notice extends ActionBarActivity {
    ProgressDialog progressDialog;
   // notice noticeObj;
   String nId,result1,result2,titles,depertments,secs,byws,dates,notxts;
    TextView ntitle,ndepertment,nsec,byW,date;
    EditText ntxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_notice);
        nId = getIntent().getExtras().getString("noticeId");
        ntitle = (TextView)findViewById(R.id.notice_title);
        ndepertment = (TextView)findViewById(R.id.department_name);
        nsec = (TextView)findViewById(R.id.notice_for_section);
        byW = (TextView)findViewById(R.id.notice_bywhome);
        date = (TextView)findViewById(R.id.notice_posting_datetime);
        ntxt = (EditText)findViewById(R.id.notice_text);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        new makePostRequest().execute();


    }



    public class makePostRequest extends AsyncTask<Void,Void,String> {

      //  HttpClient httpClient = new DefaultHttpClient();
        // replace with your url
       // HttpPost httpPost;


        @Override
        protected void onPreExecute() {
            //httpPost = new HttpPost("www.example.com");
        }

        @Override
        protected String doInBackground(Void... params) {
//
            Log.d("Show Notice activity", "noticeID = " + show_notice.this.nId.toString().trim());
            final ArrayList postParameters = new ArrayList();
            postParameters.add(new BasicNameValuePair("noticeID", show_notice.this.nId.toString().trim()));
            String response = null;

            try {
                response = serverReq.executeHttpPost("http://puc.comli.com/post_get_req.php", postParameters);
                String e = response.toString();
                result1 = "";
                JSONArray jArray = new JSONArray(e);

                for(int i = 0; i < jArray.length(); ++i) {
                    JSONObject json_data = jArray.getJSONObject(i);
                   // Log.i("log_tag", "Enrollment: " + json_data.getString("Enrollment") + ", Roll: " + json_data.getString("Roll") + ", RNO: " + json_data.getString("RNO") + ", RollID: " + json_data.getString("RollID") + ", Part: " + json_data.getString("Part") + ", CAT: " + json_data.getString("CAT") + ", FirstName: " + json_data.getString("FirstName") + ", LastName: " + json_data.getString("LastName") + ", ClassSID: " + json_data.getString("ClassSID") + ", ClassName: " + json_data.getString("ClassName") + ", ExamID: " + json_data.getString("ExamID") + ", ExamName: " + json_data.getString("ExamName") + ", ClassResult: " + json_data.getString("ClassResult"));
                    //result1 = result1 + "\n" + json_data.getString("notice_title") + " " + json_data.getString("notice_txt") + "\n" + json_data.getString("semester") + "\n" + json_data.getString("section") + "\n" + json_data.getString("department") + "\n"+ json_data.getString("dateandtime") + "\n"+ json_data.getString("by_name") + "\n";
                    titles = json_data.getString("notice_title");
                    notxts = json_data.getString("notice_txt");
                    depertments =json_data.getString("department");
                    secs = json_data.getString("section");
                    dates = json_data.getString("dateandtime");
                    byws = json_data.getString("by_name");

                }

            } catch (IOException e) {
                // Log exception
                e.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return null;
        }


            @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {

            ntitle.setText(titles);
            ndepertment.setText(depertments);
            nsec.setText(secs);
            byW.setText(byws);
            date.setText(dates);
            ntxt.setText(notxts);

            result2 = s;
            progressDialog.dismiss();
            super.onPostExecute(s);
        }







    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_notice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {

            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.share_action:
                        //to share
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
