package com.example.absjabed.puc_e_notice;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Add_notice_activity extends ActionBarActivity {

    EditText notice_title, notice_txt,notice_department,notice_semester,notice_section;
    Button add_notice;

    String snotice_title, snotice_txt,snotice_department,snotice_semester,snotice_section;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notice_activity);
        notice_title = (EditText)findViewById(R.id.notice_title_txt);
        notice_txt = (EditText)findViewById(R.id.notice_text);
        notice_department = (EditText)findViewById(R.id.department_txt);
        notice_semester = (EditText)findViewById(R.id.semester_txt);
        notice_section = (EditText)findViewById(R.id.section_txt);
        add_notice = (Button)findViewById(R.id.btn_Add_Notice);

    }

    public void onClickAdd(View view){

        snotice_title = notice_title.getText().toString();
        snotice_txt = notice_txt.getText().toString();
        snotice_department = notice_department.getText().toString();
        snotice_semester = notice_semester.getText().toString();
        snotice_section = notice_section.getText().toString();


        AddNoticeBgTask addNoticeBgTask = new AddNoticeBgTask(this);
        addNoticeBgTask.execute(snotice_title,snotice_txt,snotice_department,snotice_semester,snotice_section);
        finish();

        startActivity(new Intent(this, login_activity.class));


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_notice_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
