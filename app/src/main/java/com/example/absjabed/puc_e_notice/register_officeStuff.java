package com.example.absjabed.puc_e_notice;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class register_officeStuff extends ActionBarActivity implements View.OnClickListener {

    EditText name, department,position,phone,email,username,password;
    Button bOreg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_office_stuff);

        name = (EditText)findViewById(R.id.etName);
        department = (EditText)findViewById(R.id.etDepartment);
        position = (EditText)findViewById(R.id.etPosition);
        phone = (EditText)findViewById(R.id.etPhone);
        email = (EditText)findViewById(R.id.etEmail);
        username = (EditText)findViewById(R.id.etUsername);
        password = (EditText)findViewById(R.id.etPassword);
        bOreg = (Button)findViewById(R.id.bRegister);
        bOreg.setOnClickListener(this);

    }


  /*  public void onClickRegister(){



    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register_office_stuff, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
      /*  if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, OfficeStuff_login.class));
    }
}
