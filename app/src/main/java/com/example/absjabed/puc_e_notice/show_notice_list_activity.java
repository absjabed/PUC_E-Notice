package com.example.absjabed.puc_e_notice;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class show_notice_list_activity extends ActionBarActivity {
            String json_string,sd;
            JSONObject jsonObject;
            JSONArray jsonArray;
            ListView noticeList;
            Intent intent;
            noticeAdapter nAdapter;

    //here all objects should be created with notice id from database
    // to send it to database and load that specific notice

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_notice_list);
        noticeList = (ListView) findViewById(R.id.shownoticelistview);
        nAdapter = new noticeAdapter(this,R.layout.notice_row);
        noticeList.setAdapter(nAdapter);
        json_string = getIntent().getExtras().getString("json_data");

        try {
            jsonObject = new JSONObject(json_string);
            jsonArray = jsonObject.getJSONArray("server_response");

            int count=0;
            String noticeid,noticetxt,title,department,semester,section,datetime,byid, byname;

            while (count<jsonArray.length()){

                JSONObject JO = jsonArray.getJSONObject(count);

                //noticeid = JO.getString("noticeId");
               // noticetxt = JO.getString("noticetxt");
               // byid = JO.getString("byid");
               // byname = JO.getString("byname");
                title = JO.getString("noticetitle");
                    department = JO.getString("department");
                    semester = JO.getString("semester");
                    section = JO.getString("section");
                    datetime = JO.getString("datetime");
                    notice nw = new notice(title,department,semester,section,datetime);

                nAdapter.add(nw);

                    count++;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        noticeList.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //String catg = String.valueOf(parent.getItemAtPosition(position));

                        sd = String.valueOf(parent.getItemIdAtPosition(position+1));
                       // Toast.makeText(show_notice_list_activity.this, sd, Toast.LENGTH_SHORT).show();
                        intent = new Intent(show_notice_list_activity.this, show_notice.class);
                        intent.putExtra("noticeId", sd);
                        startActivity(intent);

                    }
                });

       // intent_sem = getIntent().getExtras().getString("message");
      //  serverRequests srvREQ = new serverRequests(this);
      //  srvREQ.getJsonBackground();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_notice_list_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //handle presses on the action bar items
        switch (item.getItemId()) {

            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.add_notice_action:
                startActivity(new Intent(this, Login_option.class));
                return true;

            case R.id.how_to_use_action:
                startActivity(new Intent(this, how_to_use.class));
                return true;

            case R.id.action_about:
                startActivity(new Intent(this, about.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}