package com.example.absjabed.puc_e_notice;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class Login_option extends ActionBarActivity  implements View.OnClickListener {

    TextView teacherL,studentL,OffieceSL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_option);
        teacherL = (TextView) findViewById(R.id.TeacherL);
        studentL =(TextView)findViewById(R.id.StudentL);
        OffieceSL = (TextView)findViewById(R.id.OfficeL);

        teacherL.setOnClickListener(this);
        studentL.setOnClickListener(this);
        OffieceSL.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.TeacherL:
                startActivity(new Intent(this, teacher_login.class));
                break;
            case R.id.StudentL:
                startActivity(new Intent(this, login_activity.class));
                break;
            case R.id.OfficeL:
                startActivity(new Intent(this, OfficeStuff_login.class));
                break;
        }

    }

 /*   public void onClickTeacherL(){


    }
   public void onClickOfficeStuffL(){


    }
    public void onClickStudentL(){


    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login_option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
