package com.example.absjabed.puc_e_notice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
public class notice_list_activity extends Activity {
    public final static String EXTRA_MESSAGE = "com.example.absjabed.puc_e_notice.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.notice_list);

   // String[] categories = {"Notice for all", "1st semester", "2nd semester", "3rd semester", "4th semester", "5th semester", "6th semester", "7th semester", "8th semester"};
    //ListAdapter catAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, categories);
    ListView noticeListView = (ListView) findViewById(R.id.NoticecatlistView);
    //noticeListView.setAdapter(catAdapter);
        final Intent intent = new Intent(this, show_notice_list_activity.class);

    noticeListView.setOnItemClickListener(
            new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String catg = String.valueOf(parent.getItemAtPosition(position));
                    Toast.makeText(notice_list_activity.this, catg, Toast.LENGTH_SHORT).show();


                    if(position == 0){
                        String message = "all";
                         intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }else if(position==1){
                        String message = "1st";
                        intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }else if(position==2){
                        String message = "2nd";
                        intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }else if(position==3){
                        String message = "3rd";
                        intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }else if(position==4){
                        String message = "4th";
                        intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }else if(position==5){
                        String message = "5th";
                        intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }else if(position==6){
                        String message = "6th";
                        intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }else if(position==7){
                        String message = "7th";
                        intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }else if(position==8){
                        String message = "8th";
                        intent.putExtra(EXTRA_MESSAGE,message);
                        startActivity(intent);
                    }
                }
            }
    );
}
}
