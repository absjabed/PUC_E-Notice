package com.example.absjabed.puc_e_notice;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by absjabed on 2015-11-20.
 */
public class BackgroundTask extends AsyncTask<String, Void, String> {
  //  AlertDialog alertDialog;
    Context ctx;

    BackgroundTask(Context ctx){
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {

 //       alertDialog = new AlertDialog.Builder(ctx).create();
   //     alertDialog.setTitle("Login information....");
    }

    @Override
    protected String doInBackground(String... params) {

        String reg_url = "http://puc.comli.com/student_register.php";
        String login_url ="http://puc.comli.com/student_login.php";

        String method = params[0];
// method,sname,sdepartment,ssemester,sstudentID,sphone,semail,susername,spassword);
        if(method.equals("register")){

            String stu_name = params[1];
            String stu_department = params[2];
            String stu_semester = params[3];
            String stu_stuID = params[4];
            String stu_phone = params[5];
            String stu_email = params[6];
            String stu_user = params[7];
            String stu_pass = params[8];

            try {
                URL url = new URL(reg_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("stu_name","UTF-8")+"="+URLEncoder.encode(stu_name,"UTF-8")+"&"+
                        URLEncoder.encode("stu_department","UTF-8")+"="+URLEncoder.encode(stu_department,"UTF-8")+"&"+
                        URLEncoder.encode("stu_semester","UTF-8")+"="+URLEncoder.encode(stu_semester,"UTF-8")+"&"+
                        URLEncoder.encode("stu_stuID","UTF-8")+"="+URLEncoder.encode(stu_stuID,"UTF-8")+"&"+
                        URLEncoder.encode("stu_phone","UTF-8")+"="+URLEncoder.encode(stu_phone,"UTF-8")+"&"+
                        URLEncoder.encode("stu_email","UTF-8")+"="+URLEncoder.encode(stu_email,"UTF-8")+"&"+
                        URLEncoder.encode("stu_user","UTF-8")+"="+URLEncoder.encode(stu_user,"UTF-8")+"&"+
                        URLEncoder.encode("stu_pass","UTF-8")+"="+URLEncoder.encode(stu_pass,"UTF-8");

                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                OS.close();
                InputStream IS = httpURLConnection.getInputStream();
                IS.close();
                httpURLConnection.disconnect();

                return "Registration Succeed.....";
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }else if(method.equals("login")){

            String login_name = params[1];
            String login_pass = params[2];

            try {
                URL url = new URL(login_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream OS = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("login_name","UTF-8")+"="+URLEncoder.encode(login_name,"UTF-8")+"&"+
                        URLEncoder.encode("login_pass","UTF-8")+"="+URLEncoder.encode(login_pass,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                OS.close();

                InputStream IS = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IS,"iso-8859-1"));
                String response = "";
                String line = "";

                while ((line = bufferedReader.readLine())!=null){

                    response = line;
                }

                bufferedReader.close();
                IS.close();
                httpURLConnection.disconnect();
                return response;



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result)
    {

        if(result.equals("Registration Succeed.....")) {
            Toast.makeText(ctx, result, Toast.LENGTH_LONG).show();
        }else{

  //          alertDialog.setMessage(result);
    //        alertDialog.show();
            ctx.startActivity(new Intent(ctx, Add_notice_activity.class));
        }

    }
}
