package com.example.absjabed.puc_e_notice;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class OfficeStuff_login extends ActionBarActivity implements View.OnClickListener {

    EditText username,password;
    Button btnLogin;
    TextView regO;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_office_stuff_login);
        username = (EditText)findViewById(R.id.etUsername);
        password = (EditText)findViewById(R.id.etPassword);
        btnLogin = (Button)findViewById(R.id.bLogin);
        regO =(TextView)findViewById(R.id.registertxt);

        btnLogin.setOnClickListener(this);
        regO.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bLogin:
                startActivity(new Intent(this, Add_notice_activity.class));
                break;
            case R.id.registertxt:
                startActivity(new Intent(this, register_officeStuff.class));
                break;
        }
    }

   /* public void onClickLogin(){

        startActivity(new Intent(this, Add_notice_activity.class));
    }

    public void onClickReg(){



    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_office_stuff_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
