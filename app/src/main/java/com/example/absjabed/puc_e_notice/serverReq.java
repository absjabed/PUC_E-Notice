package com.example.absjabed.puc_e_notice;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by absjabed on 2015-11-20.
 */
public class serverReq {

    public static final int HTTP_TIMEOUT = 30000;
    private static HttpClient mHttpClient;

    public serverReq() {
    }

    private static HttpClient getHttpClient() {
        if(mHttpClient == null) {
            mHttpClient = new DefaultHttpClient();
            HttpParams params = mHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 30000);
            HttpConnectionParams.setSoTimeout(params, 30000);
            ConnManagerParams.setTimeout(params, 30000L);
        }

        return mHttpClient;
    }

    public static String executeHttpPost(String url, ArrayList<NameValuePair> postParameters) throws Exception {
        BufferedReader in = null;

        try {
            HttpClient client = getHttpClient();
            HttpPost request = new HttpPost(url);
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
            request.setEntity(formEntity);
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");

            while((line = in.readLine()) != null) {
                sb.append(line + NL);
            }

            in.close();
            String result = sb.toString();
            String var12 = result;
            return var12;
        } finally {
            if(in != null) {
                try {
                    in.close();
                } catch (IOException var17) {
                    Log.e("log_tag", "Error converting result " + var17.toString());
                    var17.printStackTrace();
                }
            }

        }
    }

    public static String executeHttpGet(String url) throws Exception {
        BufferedReader in = null;

        try {
            HttpClient client = getHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI(url));
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");

            while((line = in.readLine()) != null) {
                sb.append(line + NL);
            }

            in.close();
            String result = sb.toString();
            String var10 = result;
            return var10;
        } finally {
            if(in != null) {
                try {
                    in.close();
                } catch (IOException var15) {
                    Log.e("log_tag", "Error converting result " + var15.toString());
                    var15.printStackTrace();
                }
            }

        }
    }

}
