package com.example.absjabed.puc_e_notice;

import android.os.AsyncTask;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
/**
 * Created by absjabed on 2015-11-21.
 */
public class AddNoticeBgTask extends AsyncTask<String, Void, String> {
        String add_notice_url;
    Context ctx;

    AddNoticeBgTask(Context ctx){

        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
        add_notice_url = "http://puc.comli.com/add_notice.php";
    }
//(snotice_title,snotice_txt,snotice_department,snotice_semester,snotice_section);
    @Override
    protected String doInBackground(String... params) {

        String title = params[0];
        String txt = params[1];
        String department = params[2];
        String semester = params[3];
        String section = params[4];

        try {
            URL url = new URL(add_notice_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            OutputStream OS = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("notice_title","UTF-8")+"="+URLEncoder.encode(title,"UTF-8")+"&"+
                    URLEncoder.encode("notice_txt","UTF-8")+"="+URLEncoder.encode(txt,"UTF-8")+"&"+
                    URLEncoder.encode("notice_department","UTF-8")+"="+URLEncoder.encode(department,"UTF-8")+"&"+
                    URLEncoder.encode("notice_semester","UTF-8")+"="+URLEncoder.encode(semester,"UTF-8")+"&"+
                    URLEncoder.encode("notice_section","UTF-8")+"="+URLEncoder.encode(section,"UTF-8");

            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            OS.close();
            InputStream IS = httpURLConnection.getInputStream();
            IS.close();
            httpURLConnection.disconnect();

            return "New notice Added.....";
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        Toast.makeText(ctx, result, Toast.LENGTH_SHORT).show();
    }




}
