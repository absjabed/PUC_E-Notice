package com.example.absjabed.puc_e_notice;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class register_student extends ActionBarActivity {

    EditText name,department,semester,studentID,phone,email,username,password;
    Button regStudent;
    String sname,sdepartment,ssemester,sstudentID,sphone,semail,susername,spassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_student);

        name = (EditText)findViewById(R.id.etName);
        department =(EditText)findViewById(R.id.etDepartment);
        semester = (EditText)findViewById(R.id.etSemester);
        studentID =(EditText)findViewById(R.id.etStudentId);
        phone = (EditText)findViewById(R.id.etPhone);
        email = (EditText)findViewById(R.id.etEmail);
        username =(EditText)findViewById(R.id.etUsername);
        password =(EditText)findViewById(R.id.etPassword);
        regStudent = (Button)findViewById(R.id.bRegister);
    }

    public void onClickReg(View view){


        sname = name.getText().toString();
        sdepartment = department.getText().toString();
        ssemester = semester.getText().toString();
        sstudentID = studentID.getText().toString();
        sphone = phone.getText().toString();
        semail = email.getText().toString();
        susername = username.getText().toString();
        spassword = password.getText().toString();

        String method = "register";
        BackgroundTask backgroundTask = new BackgroundTask(this);
        backgroundTask.execute(method,sname,sdepartment,ssemester,sstudentID,sphone,semail,susername,spassword);

        startActivity(new Intent(this, login_activity.class));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*  if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
