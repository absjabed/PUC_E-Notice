package com.example.absjabed.puc_e_notice;

import java.io.Serializable;

/**
 * Created by absjabed on 2015-11-13.
 */
@SuppressWarnings("serial")
public class notice implements Serializable {

    private String noticeTitle,noticeTxt,semester,section,department,dateandtime,byID,byName;

    public notice(String noticeTitle,String noticeTxt,String semester,String section,String department,String dateandtime,String byID,String byName ) {

        this.setNoticeTitle(noticeTitle);
        this.setNoticeTxt(noticeTxt);
        this.setSemester(semester);
        this.setSection(section);
        this.setDepartment(department);
        this.setDateandtime(dateandtime);
        this.setByID(byID);
        this.setByName(byName);
    }

    public notice(String noticeTitle,String semester,String section,String department,String dateandtime){

        this.setNoticeTitle(noticeTitle);
        this.setSemester(semester);
        this.setSection(section);
        this.setDepartment(department);
        this.setDateandtime(dateandtime);

    }

    public void setNoticeTitle(String noticeTitle) {

        this.noticeTitle = noticeTitle;
    }

    public void setNoticeTxt(String noticeTxt) {
        this.noticeTxt = noticeTxt;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setDateandtime(String dateandtime) {
        this.dateandtime = dateandtime;
    }

    public void setByID(String byID) {
        this.byID = byID;
    }

    public void setByName(String byName) {
        this.byName = byName;
    }

    public String getNoticeTitle() {

        return noticeTitle;
    }

    public String getNoticeTxt() {
        return noticeTxt;
    }

    public String getSemester() {
        return semester;
    }

    public String getSection() {
        return section;
    }

    public String getDepartment() {
        return department;
    }

    public String getDateandtime() {
        return dateandtime;
    }

    public String getByID() {
        return byID;
    }

    public String getByName() {
        return byName;
    }
}
