package com.example.absjabed.puc_e_notice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by absjabed on 2015-11-13.
 */
public class noticeAdapter extends ArrayAdapter {

    List list = new ArrayList();

    public noticeAdapter(Context context, int resource) {
        super(context, resource);
    }


    public void add(notice object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

            View row;
        row = convertView;
        noticeHolder nh;

        if(row==null){

            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.notice_row,parent,false);
            nh = new noticeHolder();

            nh.tx_title = (TextView) row.findViewById(R.id.txt_title);
            nh.tx_department = (TextView) row.findViewById(R.id.txt_department);
            nh.tx_semester = (TextView) row.findViewById(R.id.txt_semester);
            nh.tx_section = (TextView) row.findViewById(R.id.txt_section);
            nh.tx_datetime = (TextView) row.findViewById(R.id.txt_datetime);
            row.setTag(nh);

        }else{

            nh = (noticeHolder)row.getTag();

        }

        notice noticeobj = (notice) this.getItem(position);
        nh.tx_title.setText(noticeobj.getNoticeTitle());
        nh.tx_department.setText(noticeobj.getDepartment());
        nh.tx_semester.setText(noticeobj.getSemester());
        nh.tx_section.setText(noticeobj.getSection());
        nh.tx_datetime.setText(noticeobj.getDateandtime());

        return row;
    }

    static class noticeHolder{
        TextView tx_title,tx_department,tx_semester,tx_section,tx_datetime;


    }

}
